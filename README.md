## Começando
```
# Clone o projeto.
$ mkdir postcss-project
$ cd postcss-project
$ git clone https://milson_junior@bitbucket.org/milson_junior/testes_postcss.git

# Instale as dependências.
$ npm install -g gulp
$ npm install

# Gulp assiste as mudanças no código.
$ gulp watch

# Faz a transformação através de PostCSS.
$ gulp css:dev

# Faz lint do CSS.
$ gulp css:test
```