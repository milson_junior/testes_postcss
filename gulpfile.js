// --------------------------------
//	Gulpfile
// --------------------------------
//
//	Available tasks:
//
//	`gulp` or `gulp watch`
//	`gulp css:dev`
//	`gulp css:test`
//
// --------------------------------

// --------------------------------
//	Load dependencies
// --------------------------------
var postcss = require("gulp-postcss"),
	vars = require("postcss-simple-vars"),
	comments = require("postcss-inline-comment"),
	mixins = require("postcss-mixins"),
	cssextends = require("postcss-simple-extend"),
	nesting = require("postcss-nested"),
	mqpacker = require("css-mqpacker"),
	stylelint = require("stylelint"),
	reporter = require("postcss-reporter"),
	cssnext = require("gulp-cssnext"),
	rucksack = require("gulp-rucksack"),
	watch = require("gulp-watch"),
	cssimport = require("postcss-import"),
	nano = require("gulp-cssnano"),
	sourcemaps = require("gulp-sourcemaps"),
	gulp = require("gulp"),

// --------------------------------
// Config
// --------------------------------
	config = {
		dev: "dev/",
		dest: "dist/",
	},

	tasks = {

		compileCSS: function(){
			return gulp.src(config.dev + "css/*.css")
				.pipe(postcss([
					cssimport(),
					cssextends(),
					mixins(),
					nesting(),
					vars({
						silent: true
					}),
					mqpacker(),
					comments()
				]))
				.pipe(cssnext())
				.pipe(rucksack({
					fallbacks: true,
					autoprefixer: true
				}))
				.pipe(sourcemaps.init())
				.pipe(nano())
				.pipe(sourcemaps.write("."))
				.pipe(gulp.dest(config.dest + "css"))
		},

		testCSS: function(){
			return gulp.src(config.dev + "css/**/*.css")
				.pipe(postcss([
					stylelint({
						"rules": {
							"declaration-no-important": 2
						}
					}),
					reporter()
				]))
		},

		watchFiles: function(){
			gulp.watch(config.dev + "css/**/*.css", ["css:test", "css:dev"]);
		}

	};

// --------------------------------
// Tasks
// --------------------------------
gulp.task("css:dev", tasks.compileCSS);

gulp.task("css:test", tasks.testCSS);

gulp.task("watch", tasks.watchFiles);

gulp.task("default", ["watch"]);
